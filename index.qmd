---
listing:
  contents: posts
  date-format: iso
  max-description-length: 150
  feed: true 
  sort: "date desc"
  type: default
  categories: false
  sort-ui: false
  filter-ui: false
format:
  html:
    page-layout: full
title-block-banner: true
---


